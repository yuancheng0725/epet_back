const Router = require('koa-router')
const cartRouter = new Router()
const query = require('../utils/query')
const jwt = require('../utils/jwt')

// 查询购物车
cartRouter.get('/getCart',async ctx => {
    const userId = jwt.hasToken(ctx)
    if(userId){
        const sql = `select * from cart where idu = ?`
        let result = await query(sql,userId)
        if(result.length > 0){
            ctx.body = {
                code: 200,
                msg: '获取购物车信息成功',
                data: result
            }
        }else{
            ctx.body = {
                code: 201,
                msg: '用户购物车为空',
                data: result
            }
        }
    }
})

// 添加购物车
cartRouter.post('/addCart',async ctx => {
    const userId = jwt.hasToken(ctx)
    if(userId){
        const {
            goodId,
            goodName,
            goodCount,
            goodcover,
            goodMethod,
            goodColor,
            goodSize,
            goodPrice
        } = ctx.request.body
        const cid = `${goodId}${goodMethod}${goodColor}${goodSize}${userId}`
        const sql1 = `select * from cart where cid = ? && idu = ?`
        let result1 = await query(sql1,[cid,userId])
        if(result1.length === 0){
            const sql2 = `insert into cart values (?,?,?,?,?,?,?,?,?,?)`
            let result2 = await query(sql2,[cid,userId,goodName,goodCount,goodId,goodcover,goodMethod,goodColor,goodSize,goodPrice])
            if(result2.affectedRows === 1){
                ctx.body = {
                    code: 200,
                    msg: '添加购物车成功',
                }
            }else{
                ctx.body = {
                    code: 501,
                    msg: '添加购物车失败'
                }
            }
        }else{
            const sql3 = `update cart set goodCount = ? where cid = ? && idu = ?`
            const countNow = Number(result1[0].goodCount) + Number(goodCount)
            let result3 = await query(sql3,[countNow,cid,userId])
            if(result3.affectedRows === 1){
                ctx.body = {
                    code: 200,
                    msg: '添加购物车成功'
                }
            } else{
                ctx.body = {
                    code: 501,
                    msg: '添加购物车失败' 
                }
            }
        }
    }
})

// 增加或减少商品数量
cartRouter.put('/updateCount',async ctx => {
    const userId = jwt.hasToken(ctx)
    if(userId){
        const {cId,num} = ctx.request.body
        const sql1 = `select * from cart where cid = ? && idu = ?`
        let result1 = await query(sql1,[cId,userId])
        if(result1.length === 1){
            let countNow = result1[0].goodCount
            if(result1[0].goodCount + num === 0){countNow = 1}
            else {
                countNow = countNow + num
            }
            const sql2 = `update cart set goodCount = ? where cid =? && idu = ?`
            let result2 = await query(sql2,[countNow,cId,userId])
            if(result2.affectedRows === 1){
                ctx.body = {
                    code: 200,
                    msg: '操作成功',
                }
            }else{
                ctx.body = {
                    code: 501,
                    msg: '操作失败'
                }
            }
        }else{
            ctx.body = {
                code: 401,
                msg: '商品不存在'
            }
        }
    }
})


module.exports = cartRouter