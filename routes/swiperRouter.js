const Router = require('koa-router')
const swiperRouter = new Router() 
const query = require('../utils/query')

swiperRouter.get('/get',async ctx => {
    const sql = `select * from swiper`
    let result = await query(sql)
    if(result.length > 0){
        ctx.body = {
            code: 200,
            msg: '获取轮播图成功',
            data: result
        }
    }else{
        ctx.body = {
            code: 201,
            msg: '数据库中无轮播图',
            data: []
        }
    }
})

module.exports = swiperRouter
