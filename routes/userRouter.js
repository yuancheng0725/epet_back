const Router = require('koa-router')
const userRouter = new Router()
const query = require('../utils/query')
const jwt = require('../utils/jwt')
const uuid = require('uuid')
const uploadTool = require('../utils/multer')
const config = require('../config')
// 用户注册
userRouter.post('/reg',async ctx => {
    const {userAccount,userPwd,nickName} = ctx.request.body
    const sql1 = `select * from euser where userAccount = ?`
    let result1 = await query(sql1,userAccount)
    if(result1.length === 1){
        ctx.body = {
            code: 201,
            msg: '用户邮箱已被注册'
        }
    }else{
        const userId = uuid.v4()
        const sql2 = `insert into euser (userId,userAccount,userPwd,nickName) values(?,?,?,?)`
        let result2 = await query(sql2,[userId,userAccount,userPwd,nickName])
        if(result2.affectedRows === 1){
            ctx.body = {
                code: 200,
                msg: '用户注册成功'
            }
        }
    }
})

// 用户登录
userRouter.post('/login',async ctx => {
    const {userAccount,userPwd} = ctx.request.body
    const sql = `select * from euser where userPwd = ? && userAccount = ?`
    let result = await query(sql,[userPwd,userAccount])
    if(result.length === 1){
        const userId = result[0].userId
        const token = jwt.encrypt(userId)
        ctx.body = {
            code: 200,
            msg: '登录成功',
            token
        }
    }else{
        ctx.body = {
            code: 201,
            msg: '用户不存在或密码错误',
            token: ''
        }
    }
})

// 根据token获取用户信息
userRouter.get('/getUser',async ctx => {
    const userId = jwt.hasToken(ctx)
    if(userId){
        const sql = `select * from euser where userId = ?`
        let result = await query(sql,userId)
        if(result.length === 1){
            const {nickName,userId,avatar} = result[0]
            const userInfo = {
                nickName,
                avatar,
                userId
            }
            ctx.body = {
                code: 200,
                msg: '获取用户信息成功',
                data: userInfo
            }
        }else{
            ctx.body = {
                code: 201,
                msg: '获取用户信息失败',
                data: {}
            }
        }
    }
})

// 用户修改密码
userRouter.post('/changePwd',async ctx => {
    const userId = jwt.hasToken(ctx)
    if(userId){
        const {userPwd,newPwd} = ctx.request.body 
        const sql1 = `select  * from euser where userId = ?`
        let result1 = await query(sql1,userId)
        if(result1[0].userPwd === userPwd){
            const sql2 = `update euser set userPwd = ? where userId = ?`
            let result2 = await query(sql2,[newPwd,userId])
            if(result2.affectedRows === 1){
                ctx.body = {
                    code: 200,
                    msg: '修改密码成功',
                }
            }else{
                ctx.boyd = {
                    code: 201,
                    msg: '修改密码失败',
                }
            }
        }else{
            ctx.body = {
                code: 202,
                msg: '原密码不正确，无法修改',
            }
        }
    }
})

// 用户更改用户名
userRouter.get('/changeName',async ctx => {
    const userId = jwt.hasToken(ctx)
    if(userId){
        const newName = ctx.query.newName
        const sql = `update euser set nickName = ? where userId = ?`
        let result = await query(sql,[newName,userId])
        if(result.affectedRows === 1){
            ctx.body = {
                code: 200,
                msg: '修改用户名成功',
            }
        }else{
            ctx.body = {
                code: 201,
                msg: '修改用户名失败',
            }
        }
    }
})

// 用户修改头像
userRouter.post('/changeAva',uploadTool.single('uploadFile'),async ctx => {
    const userId = jwt.hasToken(ctx)
    if(userId){
        const avatar = config.url + '/petImg/avatar/' + ctx.file.filename
        const sql = `update euser set avatar = ? where userId = ?`
        let result = await query(sql,[avatar,userId])
        if(result.affectedRows === 1){
            ctx.body = {
                code: 200,
                msg: '修改头像成功',
            }
        }else{
            ctx.body = {
                code: 201,
                msg: '修改头像失败',
            }
        }
    }
})

module.exports = userRouter