const Router = require('koa-router')
const bannerRouter = new Router() 
const query = require('../utils/query')

bannerRouter.get('/get',async ctx => {
    const sql = `select * from banner`
    let result = await query(sql)
    if(result.length > 0){
        ctx.body = {
            code: 200,
            msg: '获取横幅成功',
            data: result
        }
    }else{
        ctx.body = {
            code: 201,
            msg: '数据库中无横幅',
            data: []
        }
    }
})

module.exports = bannerRouter
