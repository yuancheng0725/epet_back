const Router = require('koa-router')
const goodsRouter = new Router()
const query = require('../utils/query')

goodsRouter.get('/getAll',async ctx => {
    const sql = `select * from goods`
    let result = await query(sql)
    if(result.length > 0){
        ctx.body = {
            code: 200,
            msg: '获取所有产品信息成功',
            data: result
        }
    }else{
        ctx.body = {
            code: 201,
            msg: '数据库中产品信息为空'
        }
    }
})

module.exports = goodsRouter