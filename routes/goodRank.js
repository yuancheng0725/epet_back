const Router = require('koa-router')
const goodRankRouter = new Router()
const query = require('../utils/query')

goodRankRouter.get('/get',async ctx => {
    const sql = `select * from goodrank`
    let result = await query(sql)
    if(result.length > 0){
        ctx.body = {
            code: 200,
            msg: '查询榜单信息成功',
            data: result
        }
    }else{
        ctx.body ={
            code: 201,
            msg: '数据库信息为空',
            data: []
        }
    }
})

module.exports = goodRankRouter