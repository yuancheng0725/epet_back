const Router = require('koa-router')
const router = new Router()

const swiperRouter = require('./swiperRouter')
const bannerRouter = require('./bannerRouter')
const navbtnRouter = require('./navbtn')
const goodsRouter = require('./goodsRouter')
const goodRankRouter = require('./goodRank')
const classifyRouter = require('./classifyRouter')
const userRouter = require('./userRouter')
const cartRouter = require('./cartRouter')

router.use('/api/v1/cart',cartRouter.routes(),cartRouter.allowedMethods())
router.use('/api/v1/user',userRouter.routes(),userRouter.allowedMethods())
router.use('/api/v1/classify',classifyRouter.routes(),classifyRouter.allowedMethods())
router.use('/api/v1/goodRank',goodRankRouter.routes(),goodRankRouter.allowedMethods())
router.use('/api/v1/swiper',swiperRouter.routes(),swiperRouter.allowedMethods())
router.use('/api/v1/banner',bannerRouter.routes(),bannerRouter.allowedMethods())
router.use('/api/v1/navbtn',navbtnRouter.routes(),navbtnRouter.allowedMethods())
router.use('/api/v1/goods',goodsRouter.routes(),goodsRouter.allowedMethods())

module.exports = router