const Router = require('koa-router')
const classifyRouter = new Router()
const query = require('../utils/query')

// 获取全部一级分类
classifyRouter.get('/getfirst',async ctx => {
    const sql = `select * from classify`
    let result = await query(sql)
    if(result.length >= 0){
        ctx.body = {
            code: 200,
            msg: '获取一级分类成功',
            data: result
        }
    }else{
        ctx.body = {
            code: 201,
            msg: '数据库该数据为空',
            data: []
        }
    }
})

// 根据一级分类id获取获取二级分类
classifyRouter.get('/getsec',async ctx => {
    const sql = `select * from classifymore where classifyId = ?`
    const cId = ctx.query.classifyId
    let result = await query(sql,[cId])
    if(result.length > 0 ){
        ctx.body = {
            code: 200,
            msg: '获取二级分类成功',
            data: result
        }
    }else{
        ctx.body = {
            code: 201,
            msg: '暂无二级分类信息',
            data: []
        }
    }
})

module.exports = classifyRouter