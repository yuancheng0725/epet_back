const Router = require('koa-router')
const navbtnRouter = new Router() 
const query = require('../utils/query')

navbtnRouter.get('/get',async ctx => {
    const sql = `select * from navbtn`
    let result = await query(sql)
    if(result.length > 0){
        ctx.body = {
            code: 200,
            msg: '获取导航按钮成功',
            data: result
        }
    }else{
        ctx.body = {
            code: 201,
            msg: '无相关数据',
            data: []
        }
    }
})

module.exports = navbtnRouter
