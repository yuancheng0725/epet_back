const mysql = require('mysql')

// 创建连接池对象
const pool = mysql.createPool({
    host: '127.0.0.1',
    port: '3306',
    user: 'root',
    password: '',
    database: 'epet'
})

// 利用Promise封装数据库查询query函数
const query = (sqlStr,params) =>{
    return new Promise((resolve,reject) => {
        pool.query(sqlStr,params,(err,result) => {
            if(err){
                reject(err)
            }else{
                resolve(result)
            }
        })
    })
}

module.exports = query;