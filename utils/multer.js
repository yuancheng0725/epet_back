const multer = require('@koa/multer')
const uuid = require('uuid')

const storage = multer.diskStorage({
    destination: (req,file,cb) => {
        cb(null,'public/petImg/avatar')
    },
    filename: (req,file,cb) => {
        let name = file.originalname
        let ext = name.substr(name.lastIndexOf('.'))
        cb(null,uuid.v4() + ext)
    }
})
const uploadTool = multer({
    storage
})

module.exports = uploadTool