DROP DATABASE IF EXISTS epet;
SET  names utf8;
CREATE DATABASE epet CHARSET utf8;
USE epet;
SET  @baseUrl = 'http://127.0.0.1:8080';

CREATE TABLE euser (
    userId VARCHAR(50) PRIMARY KEY,
    userAccount VARCHAR(20),
    userPwd VARCHAR(20),
    nickName VARCHAR(10),
    avatar VARCHAR(100) DEFAULT 'http://127.0.0.1:8080/petImg/avatar/default.jpg'
);
CREATE TABLE cart (
    cid VARCHAR(100) PRIMARY KEY,
    idu VARCHAR(50),
    goodName VARCHAR(50),
    goodCount INT,
    goodId INT,
    goodcover VARCHAR(100),
    goodMethod VARCHAR(10),
    goodColor VARCHAR(10),
    goodSize VARCHAR(10),
    goodPrice DECIMAL(6,2),
    FOREIGN KEY (idu) REFERENCES euser(userId)
);
CREATE TABLE swiper (
    swiperId INT PRIMARY KEY,
    swiperImg VARCHAR(100)
);
INSERT INTO swiper VALUES (1,CONCAT(@baseUrl,'/petImg/swiper/1.jpg'));
INSERT INTO swiper VALUES (2,CONCAT(@baseUrl,'/petImg/swiper/2.jpg'));
INSERT INTO swiper VALUES (3,CONCAT(@baseUrl,'/petImg/swiper/3.jpg'));
INSERT INTO swiper VALUES (4,CONCAT(@baseUrl,'/petImg/swiper/4.jpg'));
INSERT INTO swiper VALUES (5,CONCAT(@baseUrl,'/petImg/swiper/5.jpg'));
INSERT INTO swiper VALUES (6,CONCAT(@baseUrl,'/petImg/swiper/6.jpg'));
CREATE TABLE banner (
    bannerId INT PRIMARY KEY,
    bannerImg VARCHAR(100)
);
INSERT INTO banner VALUES (1,CONCAT(@baseUrl,'/petImg/banner/1.jpg'));
INSERT INTO banner VALUES (2,CONCAT(@baseUrl,'/petImg/banner/2.jpg'));
INSERT INTO banner VALUES (3,CONCAT(@baseUrl,'/petImg/banner/3.jpg'));
CREATE TABLE navbtn (
    navbtnId INT PRIMARY KEY,
    navbtnImg VARCHAR(100)
);
INSERT INTO navbtn VALUES (1,CONCAT(@baseUrl,'/petImg/navbtn/1.gif'));
INSERT INTO navbtn VALUES (2,CONCAT(@baseUrl,'/petImg/navbtn/2.jpg'));
INSERT INTO navbtn VALUES (3,CONCAT(@baseUrl,'/petImg/navbtn/3.jpg'));
INSERT INTO navbtn VALUES (4,CONCAT(@baseUrl,'/petImg/navbtn/4.gif'));
INSERT INTO navbtn VALUES (5,CONCAT(@baseUrl,'/petImg/navbtn/5.jpg'));
INSERT INTO navbtn VALUES (6,CONCAT(@baseUrl,'/petImg/navbtn/6.jpg'));
INSERT INTO navbtn VALUES (7,CONCAT(@baseUrl,'/petImg/navbtn/7.jpg'));
INSERT INTO navbtn VALUES (8,CONCAT(@baseUrl,'/petImg/navbtn/8.jpg'));
INSERT INTO navbtn VALUES (9,CONCAT(@baseUrl,'/petImg/navbtn/9.jpg'));
INSERT INTO navbtn VALUES (10,CONCAT(@baseUrl,'/petImg/navbtn/10.jpg'));
CREATE TABLE classify (
    classId INT PRIMARY KEY,
    className VARCHAR(10)
);
INSERT INTO classify VALUES (1,'猫咪主粮');
INSERT INTO classify VALUES (2,'猫咪零食');
INSERT INTO classify VALUES (3,'猫咪清洁');
INSERT INTO classify VALUES (4,'猫咪日用');
INSERT INTO classify VALUES (5,'猫咪保健');
INSERT INTO classify VALUES (6,'猫咪护理');
INSERT INTO classify VALUES (7,'猫咪玩具');
INSERT INTO classify VALUES (8,'猫咪洗浴');
INSERT INTO classify VALUES (9,'猫咪装扮');
INSERT INTO classify VALUES (10,'猫咪出行');
CREATE TABLE classifyMore (
    moreId INT PRIMARY KEY,
    classifyId INT,
    moreName VARCHAR(20),
    moreImg VARCHAR(100),
    FOREIGN KEY (classifyId) REFERENCES classify(classId)
);
INSERT INTO classifymore VALUES (1,1,'进口猫粮',CONCAT(@baseUrl,'/petImg/moreclass/1.jpg'));
INSERT INTO classifymore VALUES (2,1,'国产猫粮',CONCAT(@baseUrl,'/petImg/moreclass/2.jpg'));
INSERT INTO classifymore VALUES (3,1,'功能猫粮',CONCAT(@baseUrl,'/petImg/moreclass/3.jpg'));
INSERT INTO classifymore VALUES (4,1,'主食罐',CONCAT(@baseUrl,'/petImg/moreclass/4.jpg'));
INSERT INTO classifymore VALUES (5,2,'罐头湿粮',CONCAT(@baseUrl,'/petImg/moreclass/5.jpg'));
INSERT INTO classifymore VALUES (6,2,'美味点心',CONCAT(@baseUrl,'/petImg/moreclass/6.jpg'));
INSERT INTO classifymore VALUES (7,2,'肉制零食',CONCAT(@baseUrl,'/petImg/moreclass/7.jpg'));
INSERT INTO classifymore VALUES (8,2,'猫草薄荷',CONCAT(@baseUrl,'/petImg/moreclass/8.jpg'));
INSERT INTO classifymore VALUES (9,3,'猫咪猫砂',CONCAT(@baseUrl,'/petImg/moreclass/9.jpg'));
INSERT INTO classifymore VALUES (10,3,'猫咪厕所',CONCAT(@baseUrl,'/petImg/moreclass/10.jpg'));
INSERT INTO classifymore VALUES (11,3,'清洁除臭',CONCAT(@baseUrl,'/petImg/moreclass/11.jpg'));
INSERT INTO classifymore VALUES (12,4,'猫咪餐具',CONCAT(@baseUrl,'/petImg/moreclass/12.jpg'));
INSERT INTO classifymore VALUES (13,4,'猫咪住所',CONCAT(@baseUrl,'/petImg/moreclass/13.jpg'));
INSERT INTO classifymore VALUES (14,4,'主人专享',CONCAT(@baseUrl,'/petImg/moreclass/14.jpg'));
INSERT INTO classifymore VALUES (15,5,'强化免疫',CONCAT(@baseUrl,'/petImg/moreclass/15.jpg'));
INSERT INTO classifymore VALUES (16,5,'补钙强骨',CONCAT(@baseUrl,'/petImg/moreclass/16.jpg'));
INSERT INTO classifymore VALUES (17,5,'微量元素',CONCAT(@baseUrl,'/petImg/moreclass/17.jpg'));
INSERT INTO classifymore VALUES (18,5,'美毛化毛',CONCAT(@baseUrl,'/petImg/moreclass/18.jpg'));
INSERT INTO classifymore VALUES (19,5,'肠胃调理',CONCAT(@baseUrl,'/petImg/moreclass/19.jpg'));
INSERT INTO classifymore VALUES (20,6,'安全驱虫',CONCAT(@baseUrl,'/petImg/moreclass/20.jpg'));
INSERT INTO classifymore VALUES (21,6,'五官护理',CONCAT(@baseUrl,'/petImg/moreclass/21.jpg'));
INSERT INTO classifymore VALUES (22,6,'皮肤护理',CONCAT(@baseUrl,'/petImg/moreclass/22.jpg'));
INSERT INTO classifymore VALUES (23,6,'家庭常备',CONCAT(@baseUrl,'/petImg/moreclass/23.jpg'));
INSERT INTO classifymore VALUES (24,7,'逗猫棒/杆',CONCAT(@baseUrl,'/petImg/moreclass/24.jpg'));
INSERT INTO classifymore VALUES (25,7,'磨爪玩具',CONCAT(@baseUrl,'/petImg/moreclass/25.jpg'));
INSERT INTO classifymore VALUES (26,7,'爬架/家具',CONCAT(@baseUrl,'/petImg/moreclass/26.jpg'));
INSERT INTO classifymore VALUES (27,7,'休闲玩具',CONCAT(@baseUrl,'/petImg/moreclass/27.jpg'));
INSERT INTO classifymore VALUES (28,8,'日常洗护',CONCAT(@baseUrl,'/petImg/moreclass/28.jpg'));
INSERT INTO classifymore VALUES (29,8,'梳剪工具',CONCAT(@baseUrl,'/petImg/moreclass/29.jpg'));
INSERT INTO classifymore VALUES (30,8,'洗澡配套',CONCAT(@baseUrl,'/petImg/moreclass/30.jpg'));
INSERT INTO classifymore VALUES (31,9,'时尚服饰',CONCAT(@baseUrl,'/petImg/moreclass/31.jpg'));
INSERT INTO classifymore VALUES (32,9,'精美配饰',CONCAT(@baseUrl,'/petImg/moreclass/32.jpg'));
INSERT INTO classifymore VALUES (33,9,'温暖窝垫',CONCAT(@baseUrl,'/petImg/moreclass/33.jpg'));
INSERT INTO classifymore VALUES (34,10,'外出餐具',CONCAT(@baseUrl,'/petImg/moreclass/34.jpg'));
INSERT INTO classifymore VALUES (35,10,'猫咪牵引',CONCAT(@baseUrl,'/petImg/moreclass/35.jpg'));
INSERT INTO classifymore VALUES (36,10,'外出用品',CONCAT(@baseUrl,'/petImg/moreclass/36.jpg'));
INSERT INTO classifymore VALUES (37,10,'放走丢',CONCAT(@baseUrl,'/petImg/moreclass/37.jpg'));
CREATE TABLE goodRank (
    rankId INT PRIMARY KEY,
    rankName VARCHAR(20),
    rankDesc VARCHAR(50)
);
INSERT INTO goodRank VALUES (1,'国产猫粮热销榜','按照商品最近30天自然销售排序');
INSERT INTO goodRank VALUES (2,'WDJ权威推荐榜','源于进口粮优选指南');
INSERT INTO goodRank VALUES (3,'猫粮回购热榜','仅参考站内数据');
INSERT INTO goodRank VALUES (4,'猫猫长胖要吃什么粮','猫肥家润');
INSERT INTO goodRank VALUES (5,'高"回头率"好食榜','买买买不停的品质粮');
INSERT INTO goodRank VALUES (6,'猫猫喜爱榜','按照猫猫通用用户最近30天购买商品排序');

CREATE TABLE goods (
    goodId INT PRIMARY KEY,
    goodClass INT,
    goodMore INT,
    goodRank INT,
    gname VARCHAR(50),
    gdesc VARCHAR(100),
    gcover VARCHAR(100),
    gprice DECIMAL(6,2),
    gimgs VARCHAR(500),
    gsize VARCHAR(100),
    gmethod VARCHAR(100),
    gcolor VARCHAR(100),
    gvender VARCHAR(30),
    gvenderdesc VARCHAR(50),
    gvenderImg VARCHAR(100)
);

INSERT INTO goods VALUES (
    1,1,1,2,
    '新西兰原装进口 滋益巅峰Ziwi Peak 风干牛肉配方猫粮',
    '新西兰原装进口 丰富肉含量 无麸无谷低敏护肠胃 双重风干适口性佳',
    CONCAT(@baseUrl,'/petImg/goods/1/cover.jpg'),
    430.00,
    CONCAT(@baseUrl,'/petImg/goods/1/img1.jpg',',',@baseUrl,'/petImg/goods/1/img2.jpg',',',@baseUrl,'/petImg/goods/1/img3.jpg',',',@baseUrl,'/petImg/goods/1/img4.jpg'),
    '0.4KG:205,1KG:430',
    '牛肉',
    '',
    '滋益巅峰Ziwi Peak',
    '无与伦比的爱，只为它',
    CONCAT(@baseUrl,'/petImg/goods/1/vender.jpg')
);
INSERT INTO goods VALUES (
    2,1,1,0,
    '加拿大原装进口枫趣Pronature 盛宴系列 鸭肉鲜橙无谷配方 成猫粮',
    '适合1岁以上成年猫食用 65%鲜肉&35%果蔬 有益皮毛健康 降低血中胆固醇',
    CONCAT(@baseUrl,'/petImg/goods/2/cover.jpg'),
    560.00,
    CONCAT(@baseUrl,'/petImg/goods/2/img1.jpg',',',@baseUrl,'/petImg/goods/2/img2.jpg',',',@baseUrl,'/petImg/goods/2/img3.jpg',',',@baseUrl,'/petImg/goods/2/img4.jpg'),
    '5.44KG:560,2.72KG:320,0.34KG:60',
    '鸭肉',
    '',
    '枫趣Pronature',
    'Pronature Holistic',
    CONCAT(@baseUrl,'/petImg/goods/2/vender.jpg')
);
INSERT INTO goods VALUES (
    3,1,1,4,
    '加拿大原装进口枫趣Pronature 莱芙系列鸡肉&火鸡肉配方全猫粮',
    '适合各阶段猫咪食用 健康低卡 舒缓爱宠情绪 维持体态',
    CONCAT(@baseUrl,'/petImg/goods/3/cover.jpg'),
    238.00,
    CONCAT(@baseUrl,'/petImg/goods/3/img1.jpg'),
    '2.27KG:238',
    '鸡肉&火鸡肉',
    '',
    '枫趣Pronature',
    'Pronature Holistic',
    CONCAT(@baseUrl,'/petImg/goods/3/vender.jpg')
);
INSERT INTO goods VALUES (
    4,1,1,0,
    '新西兰原装进口 卫塔卡夫Vitakraft 含羊肉鸡肉配方猫粮',
    '无糖配方 无谷低敏 90%动物蛋白 呵护肠胃易消化',
    CONCAT(@baseUrl,'/petImg/goods/4/cover.jpg'),
    199.00,
    CONCAT(@baseUrl,'/petImg/goods/4/img1.jpg',',',@baseUrl,'/petImg/goods/4/img2.jpg',',',@baseUrl,'/petImg/goods/4/img3.jpg',',',@baseUrl,'/petImg/goods/4/img4.jpg'),
    '1.8KG:199,7KG:578',
    '羊肉&鸡肉',
    '',
    '卫塔卡夫',
    'Vitakraft',
    CONCAT(@baseUrl,'/petImg/goods/4/vender.jpg')
);
INSERT INTO goods VALUES (
    5,1,1,5,
    '加拿大原装进口 爱肯拿Acana 无谷鸡肉鱼配方全猫粮',
    '渴望同厂出品 75%肉类含量 无谷配方 原料新鲜',
    CONCAT(@baseUrl,'/petImg/goods/5/cover.jpg'),
    570.00,
    CONCAT(@baseUrl,'/petImg/goods/5/img1.jpg',',',@baseUrl,'/petImg/goods/5/img2.jpg',',',@baseUrl,'/petImg/goods/5/img3.jpg',',',@baseUrl,'/petImg/goods/5/img4.jpg'),
    '1.8KG:235,5.4KG:570,1KG:145',
    '羊肉&鸡肉',
    '',
    '爱肯拿',
    'ACANA',
    CONCAT(@baseUrl,'/petImg/goods/5/vender.jpg')
);
INSERT INTO goods VALUES (
    6,1,1,2,
    '美国原装进口 Instinct生鲜本能百利猫粮/无谷系列 鸡肉配方全猫粮',
    '中国B2C平台合作伙伴 高蛋白 易消化 生鲜肉涂层 更美味',
    CONCAT(@baseUrl,'/petImg/goods/6/cover.jpg'),
    570.00,
    CONCAT(@baseUrl,'/petImg/goods/6/img1.jpg',',',@baseUrl,'/petImg/goods/6/img2.jpg',',',@baseUrl,'/petImg/goods/6/img3.jpg',',',@baseUrl,'/petImg/goods/6/img4.jpg'),
    '5KG:468',
    '鸡肉',
    '',
    '生鲜本能Instinct',
    '纯真生食 生鲜本能',
    CONCAT(@baseUrl,'/petImg/goods/6/vender.jpg')
);
INSERT INTO goods VALUES (
    7,1,1,0,
    '加拿大原装进口原始猎食渴望Orijen 低卡路里无谷鸡肉减肥猫粮',
    '肉含量85% 鲜肉饮食 低卡路里 维持体态',
    CONCAT(@baseUrl,'/petImg/goods/7/cover.jpg'),
    285.00,
    CONCAT(@baseUrl,'/petImg/goods/7/img1.jpg',',',@baseUrl,'/petImg/goods/7/img2.jpg',',',@baseUrl,'/petImg/goods/7/img3.jpg',',',@baseUrl,'/petImg/goods/7/img4.jpg'),
    '1.8KG:285,5.4KG:650',
    '鸡肉',
    '',
    '原始猎食渴望Orijen',
    '符合生物学性哲学',
    CONCAT(@baseUrl,'/petImg/goods/7/vender.jpg')
);
INSERT INTO goods VALUES (
    8,1,2,0,
    '小青鲨super shark 无谷冻干双拼成猫粮',
    '100%小鲨鱼鲜肉冻干 零谷物添加 富含益生菌 护胃促消化',
    CONCAT(@baseUrl,'/petImg/goods/8/cover.jpg'),
    197.00,
    CONCAT(@baseUrl,'/petImg/goods/8/img1.jpg',',',@baseUrl,'/petImg/goods/8/img2.jpg',',',@baseUrl,'/petImg/goods/8/img3.jpg',',',@baseUrl,'/petImg/goods/8/img4.jpg'),
    '1.8KG:197,5.5KG:418',
    '鸡肉',
    '',
    '小青鲨',
    'super shark',
    CONCAT(@baseUrl,'/petImg/goods/8/vender.jpg')
);
INSERT INTO goods VALUES (
    9,1,2,5,
    '雷米高 澳丽得 海洋鱼味全猫粮',
    '精选海洋鱼 香脆可口 营养均衡 美毛亮毛 满足挑嘴猫',
    CONCAT(@baseUrl,'/petImg/goods/9/cover.jpg'),
    6.80,
    CONCAT(@baseUrl,'/petImg/goods/9/img1.jpg',',',@baseUrl,'/petImg/goods/9/img2.jpg',',',@baseUrl,'/petImg/goods/9/img3.jpg',',',@baseUrl,'/petImg/goods/9/img4.jpg'),
    '500G:6.8,10KG:119',
    '鸡肉',
    '',
    '雷米高',
    'RAMICAL',
    CONCAT(@baseUrl,'/petImg/goods/9/vender.jpg')
);
INSERT INTO goods VALUES (
    10,1,2,1,
    '伯纳天纯Pure&Natural 添加金枪鱼马铃薯蔓越莓成猫粮',
    '含车前子 防止毛球形成 添加蔓越莓 呵护泌尿系统健康',
    CONCAT(@baseUrl,'/petImg/goods/10/cover.jpg'),
    309.00,
    CONCAT(@baseUrl,'/petImg/goods/10/img1.jpg',',',@baseUrl,'/petImg/goods/10/img2.jpg',',',@baseUrl,'/petImg/goods/10/img3.jpg',',',@baseUrl,'/petImg/goods/10/img4.jpg'),
    '1.5KG:79,10KG:309',
    '鸡肉&金枪鱼',
    '',
    '伯纳天纯Pure&Natural',
    '生命之芯，源自天纯',
    CONCAT(@baseUrl,'/petImg/goods/10/vender.jpg')
);
INSERT INTO goods VALUES (
    11,1,2,6,
    '玫斯METZ 无谷物全猫粮',
    '70%含肉量 清火气 缓解泪痕 助消化 营养好吸收无谷鲜肉配方 特别添加EPA、DHA 优秀长肉粮食',
    CONCAT(@baseUrl,'/petImg/goods/11/cover.jpg'),
    334.00,
    CONCAT(@baseUrl,'/petImg/goods/11/img1.jpg'),
    '15磅:334,3磅:88',
    '鸡肉',
    '',
    '玫斯',
    'METZ',
    CONCAT(@baseUrl,'/petImg/goods/11/vender.png')
);
INSERT INTO goods VALUES (
    12,1,2,1,
    '信元发育宝 饕餮肉宴系列六种肉冻干成猫粮',
    '无玉米小麦 6种冻干肉粒 满足肉食天性的饕餮肉宴',
    CONCAT(@baseUrl,'/petImg/goods/12/cover.jpg'),
    319.00,
    CONCAT(@baseUrl,'/petImg/goods/12/img1.jpg',',',@baseUrl,'/petImg/goods/12/img2.jpg',',',@baseUrl,'/petImg/goods/12/img3.jpg',',',@baseUrl,'/petImg/goods/12/img4.jpg'),
    '8KG:319,1.5KG:119',
    '鸡肉',
    '',
    '信元发育宝',
    'XYFYB',
    CONCAT(@baseUrl,'/petImg/goods/12/vender.jpg')
);
INSERT INTO goods VALUES (
    13,1,3,3,
    '比瑞吉Nature Bridge 无谷草本系列 胃肠道全期猫粮',
    '添加山楂+山药 促进肠胃道蠕动 冻干技术保留食材本真',
    CONCAT(@baseUrl,'/petImg/goods/13/cover.jpg'),
    119.00,
    CONCAT(@baseUrl,'/petImg/goods/13/img1.jpg',',',@baseUrl,'/petImg/goods/13/img2.jpg',',',@baseUrl,'/petImg/goods/13/img3.jpg',',',@baseUrl,'/petImg/goods/13/img4.jpg'),
    '2KG:119',
    '鸡肉&鱼肉',
    '',
    '比瑞吉Nature Bridge',
    '天然调理 相伴更久',
    CONCAT(@baseUrl,'/petImg/goods/13/vender.jpg')
);
INSERT INTO goods VALUES (
    14,1,3,4,
    '醇粹Purich 优选成猫去毛球猫粮',
    '养发护毛 缓解掉毛 适用于12月龄以上猫咪',
    CONCAT(@baseUrl,'/petImg/goods/14/cover.jpg'),
    179.00,
    CONCAT(@baseUrl,'/petImg/goods/14/img1.jpg',',',@baseUrl,'/petImg/goods/14/img2.jpg',',',@baseUrl,'/petImg/goods/14/img3.jpg',',',@baseUrl,'/petImg/goods/14/img4.jpg'),
    '10KG:179',
    '鸡肉&鱼肉',
    '',
    '醇粹',
    'Purich',
    CONCAT(@baseUrl,'/petImg/goods/14/vender.png')
);
INSERT INTO goods VALUES (
    15,1,3,1,
    '美食厨房 天然珍萃系列 敏感肠道配方室内成猫粮',
    '调理肠胃 增加吸收 补充营养',
    CONCAT(@baseUrl,'/petImg/goods/15/cover.jpg'),
    75.00,
    CONCAT(@baseUrl,'/petImg/goods/15/img1.jpg',',',@baseUrl,'/petImg/goods/15/img2.jpg',',',@baseUrl,'/petImg/goods/15/img3.jpg',',',@baseUrl,'/petImg/goods/15/img4.jpg'),
    '1.8KG:75',
    '鸡肉&鱼肉',
    '',
    '美食厨房',
    'foodKitchen',
    CONCAT(@baseUrl,'/petImg/goods/15/vender.jpg')
);
INSERT INTO goods VALUES (
    16,1,3,3,
    '艾尔Aier 鸭肉+蔓越莓配方全期猫粮',
    '调理肠胃 增加吸收 补充营养',
    CONCAT(@baseUrl,'/petImg/goods/16/cover.jpg'),
    101.00,
    CONCAT(@baseUrl,'/petImg/goods/16/img1.jpg',',',@baseUrl,'/petImg/goods/16/img2.jpg',',',@baseUrl,'/petImg/goods/16/img3.jpg',',',@baseUrl,'/petImg/goods/16/img4.jpg'),
    '1.5KG:101',
    '鸭肉',
    '',
    '艾尔',
    'Aier',
    CONCAT(@baseUrl,'/petImg/goods/16/vender.jpg')
);
INSERT INTO goods VALUES (
    17,1,4,6,
    '德国原装进口OCANIS 兔肉配方全猫罐',
    '全猫可食用 98.9%高含肉量 补充营养 适口性佳',
    CONCAT(@baseUrl,'/petImg/goods/17/cover.jpg'),
    17.90,
    CONCAT(@baseUrl,'/petImg/goods/17/img1.jpg'),
    '200G:17.9',
    '兔肉',
    '',
    'OCANIS',
    '德国进口',
    CONCAT(@baseUrl,'/petImg/goods/17/vender.png')
);
INSERT INTO goods VALUES (
    18,1,4,2,
    '阿飞和巴弟 鸡肉配方猫咪主食罐',
    '适合4个月以上猫用 去汤汁后含肉量≥96% 有助补充水分 满足食肉天性',
    CONCAT(@baseUrl,'/petImg/goods/18/cover.jpg'),
    13.90,
    CONCAT(@baseUrl,'/petImg/goods/18/img1.jpg',',',@baseUrl,'/petImg/goods/18/img2.jpg',',',@baseUrl,'/petImg/goods/18/img3.jpg'),
    '170G:13.9',
    '鸡肉',
    '',
    '阿飞和巴弟',
    'Alfie&Buddy',
    CONCAT(@baseUrl,'/petImg/goods/18/vender.png')
);
INSERT INTO goods VALUES (
    19,1,4,4,
    '新西兰原装进口K9Natural 天然无谷猫罐头 牛肉鳕鱼',
    '天然肉类 生食大餐 无转基因 无大豆玉米 低敏',
    CONCAT(@baseUrl,'/petImg/goods/19/cover.jpg'),
    19.90,
    CONCAT(@baseUrl,'/petImg/goods/19/img1.jpg'),
    '85G:19.9',
    '牛肉+鳕鱼',
    '',
    'K9Natural',
    '独树一帜冻干粮',
    CONCAT(@baseUrl,'/petImg/goods/19/vender.jpg')
);
INSERT INTO goods VALUES (
    20,1,4,0,
    '意大利原装进口Gemon 极萌湿粮系列 金枪鱼味绝育猫餐盒',
    '适合术后绝育猫食用 细腻鱼肉 美味多汁 多吃不长胖',
    CONCAT(@baseUrl,'/petImg/goods/20/cover.jpg'),
    9.90,
    CONCAT(@baseUrl,'/petImg/goods/20/img1.jpg',',',@baseUrl,'/petImg/goods/20/img2.jpg',',',@baseUrl,'/petImg/goods/20/img3.jpg',',',@baseUrl,'/petImg/goods/20/img4.jpg'),
    '100G:9.9',
    '金枪鱼',
    '',
    'Gemon',
    '源自1963',
    CONCAT(@baseUrl,'/petImg/goods/20/vender.jpg')
);
INSERT INTO goods VALUES (
    21,1,4,0,
    '不然呢primo 鸡肉+三文鱼膳食幼猫罐',
    '新西兰进口幼猫罐 无谷低敏配方 呵护肠胃 适口易消化',
    CONCAT(@baseUrl,'/petImg/goods/21/cover.jpg'),
    19.90,
    CONCAT(@baseUrl,'/petImg/goods/21/img1.jpg',',',@baseUrl,'/petImg/goods/21/img2.jpg',',',@baseUrl,'/petImg/goods/21/img3.jpg',',',@baseUrl,'/petImg/goods/21/img4.jpg'),
    '175G:19.9',
    '鸡肉+三文鱼',
    '',
    '不然呢',
    'Primo',
    CONCAT(@baseUrl,'/petImg/goods/21/vender.jpg')
);